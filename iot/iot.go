package iot

import (
	"encoding/json"
	"fmt"
	"iot-mqtt-api/db"
	"iot-mqtt-api/logger"
	"iot-mqtt-api/mqtt"
	"log"
	"net/http"
	"strconv"
)

type IOT struct {
	database *db.Database
	mqtt     *mqtt.MQTT
	log      *logger.Logger
	data     Data
}

type Data struct {
	Temperature float64 `json:"temperature"`
	Humidity	float64	`json:"humidity"`
	Distance    float64 `json:"distance"`
}

func NewIOT(d *db.Database, m *mqtt.MQTT, l *logger.Logger) *IOT {
	return &IOT{database: d, mqtt: m, log: l}
}

func (iot *IOT) ReceiveData() {
	for {
		message := <-iot.mqtt.Messages
		switch message.Topic() {
		case "iot/dpetrec/temperatura":
			iot.data.Temperature, _ = strconv.ParseFloat(string(message.Payload()), 64)
			iot.insertTemperatureInDB(iot.data.Temperature)
		case "iot/dpetrec/vlaga":
			iot.data.Humidity, _ = strconv.ParseFloat(string(message.Payload()), 64)
			iot.insertHumidityInDB(iot.data.Humidity)
		case "iot/dpetrec/udaljenost":
			iot.data.Distance, _ = strconv.ParseFloat(string(message.Payload()), 64)
			iot.insertDistanceInDB(iot.data.Distance)
		}
		fmt.Printf("%s - %s\n", message.Topic(), message.Payload())
		}
	}

func (iot *IOT) WebServer() {
	http.HandleFunc("/data", iot.getData)
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func (iot *IOT) getData(w http.ResponseWriter, r *http.Request) {
	jsonData, _ := json.Marshal(iot.data)
	fmt.Fprintf(w, "%s", jsonData)
}

func (iot *IOT) insertTemperatureInDB(temperature float64) {
	_, err := iot.database.Conn.Exec("INSERT INTO Temperatures (Value, InsertedAt) VALUES ($1,CURRENT_TIMESTAMP)", temperature)
	if err != nil {
		log.Println(err)
	}
}

func (iot *IOT) insertHumidityInDB(humidity float64) {
	_, err := iot.database.Conn.Exec("INSERT INTO Humidities (Value, InsertedAt) VALUES ($1, CURRENT_TIMESTAMP)", humidity)
	if err != nil {
		log.Println(err)
	}
}

func (iot *IOT) insertDistanceInDB(distance float64) {
	_, err := iot.database.Conn.Exec("INSERT INTO Distances (Value, InsertedAt) VALUES ($1, CURRENT_TIMESTAMP)", distance)
	if err != nil {
		log.Println(err)
	}
}

func enableCors(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
}
